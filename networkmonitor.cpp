#include "networkmonitor.h"


NetworkMonitor::NetworkMonitor(const std::string &ep)
{
	std::string endpoint = ep;
	CreateChannel(endpoint);
	GetBytesPerSecond();
	GetPacketsPerSecond();
}

void NetworkMonitor::CreateChannel(const std::string &ep)
{
	using grpc::Channel;
	std::shared_ptr<Channel> channel;
	channel = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	stub = NetworkMonitoring::NewStub(channel);
}

mon::AllIfaceResponses NetworkMonitor::GetBytesPerSecond()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusIface;
	mon::AllIfaceResponses allIfaceRespCtrl;
	if (!stub)
		return allIfaceRespCtrl;
	statusIface = stub->GetBytesPerSecond(&context, empty, &allIfaceResp); 
	return allIfaceResp;
}

mon::IfaceResponse NetworkMonitor::GetIfaceBytesPerSecond(std::string ifacename)
{
	grpc::ClientContext context;
	grpc::Status statusIfaceBytes;
	mon::IfaceResponse ifaceResponseCtrl;
	mon::IfaceResponse ifaceResponse;
	mon::IfaceQ ifaceQ;
	if (!stub)
		return ifaceResponseCtrl;
	*ifaceQ.mutable_name() = ifacename;
	statusIfaceBytes = stub->GetIfaceBytesPerSecond(&context, ifaceQ, &ifaceResponse);
	return ifaceResponse;
}

mon::AllIfaceResponses NetworkMonitor::GetPacketsPerSecond()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusPackets;
	mon::AllIfaceResponses allPacketsRespCtrl;
	mon::AllIfaceResponses allPacketsResp;
	if (!stub)
		return allPacketsRespCtrl;
	statusPackets = stub->GetPacketsPerSecond(&context, empty, &allPacketsResp);
	return allPacketsResp;
}

mon::IfaceResponse NetworkMonitor::GetIfacePacketsPerSecond(std::string ifacename)
{
	grpc::ClientContext context;
	grpc::Status statusIfaceBytes;
	mon::IfaceResponse ifacePacketResponseCtrl;
	mon::IfaceResponse ifacePacketResponse;
	mon::IfaceQ ifaceQ;
	mon::IfaceValue ifaceValue;
	if (!stub)
		return ifacePacketResponseCtrl;
	*ifaceQ.mutable_name() = ifacename;
	statusIfaceBytes = stub->GetIfacePacketsPerSecond(&context, ifaceQ, &ifacePacketResponse);
	return ifacePacketResponse;
}

NetworkMonitor::~NetworkMonitor()
{
	
}
