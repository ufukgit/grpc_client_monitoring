#ifndef DISKMONITOR_H
#define DISKMONITOR_H

#include <grpc++/grpc++.h>
#include "monitoring.grpc.pb.h"
#include <google/protobuf/empty.pb.h>
#include <QObject>
#include <QDebug>

using mon::DiskMonitoring;

class DiskMonitor : public QObject
{
	Q_OBJECT
public:
	DiskMonitor(const std::string &ep);
	~DiskMonitor();
	mon::AllDiskResponse allDiskResp;
	mon::DiskResponse GetDiskBPS(std::string diskname);
	mon::DiskResponse GetDiskIOPS(std::string diskname);
	mon::DiskResponse GetDiskRequestSizeInBytesPerRead(std::string diskname);
private:
	void CreateChannel(const std::string &ep);
	mon::AllDiskResponse GetBPS();
	mon::AllDiskResponse GetIOPS();
	mon::AllDiskResponse GetRequestSizeInBytesPerRead();
	std::unique_ptr<DiskMonitoring::Stub> stub = nullptr;
};

#endif // DISKMONITOR_H
