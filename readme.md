I used the method on a remote server on my own client using the gRPC protocol. gRPC is a framework that speeds up the client-server relationship. First, the monitoring.proto file was generated. In this file, there are messages containing the disks of the server and the read and write speeds of these disks. There are messages containing the interfaces and the bandwidth values ​​of these interfaces. There are messages showing memory usage. There are messages showing core count and core usage. The required services and messages were generated. Then a channel was created to connect to the server. Created context named 'stub' to call rpc's in proto file. The desired values ​​were obtained according to the functions in the services. The project was completed by assigning the obtained values ​​to the chart graph.

When the user runs the program;
The user enters the IP and port information of the server he wants to connect to.
If the server is broadcasting according to the entered address, the disks are listed in the diskmonitor tab and the user selects the disk he wants and gets the read and write speed of the disk.
In the Networkmonitor tab, interfaces are listed and the user selects the interface he wants and gets the read and write speeds of the interface.
Memorymonitor tab shows memory usage.
Cpumonitor tab, the number of cores and core usage are displayed.
