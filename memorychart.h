#ifndef MEMORYCHART_H
#define MEMORYCHART_H

#include <QObject>
#include <QPieSeries>
#include <QtCharts>
#include <QPieSlice>

class MemoryChart : public QObject
{
	Q_OBJECT
public:
	MemoryChart(QObject *parent, QWidget *chartFree, QWidget *chartUsed, QWidget *chartCached, QWidget *chartSwap);
	~MemoryChart();
	QPieSeries *seriesFree;
	QChartView *chartViewMem;
	QPieSlice *slice;
	QPieSeries *seriesUsed;
	QChartView *chartViewMemUsed;
	QPieSlice *sliceUsed;
	QPieSeries *seriesCached;
	QChartView *chartViewMemCached;
	QPieSlice *sliceCached;
	QPieSeries *seriesSwap;
	QChartView *chartViewMemSwap;
	QPieSlice *sliceSwap;
	void createChartMem();
private:
	QWidget *uiChartFree;
	QWidget *uiChartUsed;
	QWidget *uiChartCached;
	QWidget *uiChartSwap;
	
	
};

#endif // MEMORYCHART_H
