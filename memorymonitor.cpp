#include "memorymonitor.h"

MemoryMonitor::MemoryMonitor(const std::string &ep)
{
	std::string endpoint = ep;
	CreateChannel(endpoint);
}

void MemoryMonitor::CreateChannel(const std::string &ep)
{
	using grpc::Channel;
	std::shared_ptr<Channel> channel;
	channel = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	stub = MemoryMonitoring::NewStub(channel);
}

mon::FreeMemory MemoryMonitor::GetFreeMemory()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusFree;
	mon::FreeMemory freeMemoryCtrl;
	mon::FreeMemory freeMemoryResp;
	if (!stub)
		return freeMemoryCtrl;
	statusFree = stub->GetFreeMemory(&context, empty, &freeMemoryResp);
	return freeMemoryResp;
}

mon::UsedMemory MemoryMonitor::GetUsedMemory()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusUsed;
	mon::UsedMemory usedMemoryCtrl;
	mon::UsedMemory usedMemoryResp;
	if (!stub)
		return usedMemoryCtrl;
	statusUsed = stub->GetUsedMemory(&context, empty, &usedMemoryResp);
	return usedMemoryResp;
}

mon::CachedMemory MemoryMonitor::GetCachedMemory()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusCached;
	mon::CachedMemory cachedMemoryCtrl;
	mon::CachedMemory cachedMemoryResp;
	if (!stub)
		return cachedMemoryCtrl;
	statusCached = stub->GetCachedMemory(&context, empty, &cachedMemoryResp);
	return cachedMemoryResp;
}

mon::SwapMemory MemoryMonitor::GetSwapMemory()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusSwap;
	mon::SwapMemory swapMemoryCtrl;
	mon::SwapMemory swapMemoryResp;
	if (!stub)
		return swapMemoryCtrl;
	statusSwap = stub->GetSwapMemory(&context, empty, &swapMemoryResp);
	return swapMemoryResp;
}

mon::TotalMemory MemoryMonitor::GetTotalMemory()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusTotal;
	mon::TotalMemory totalMemoryCtrl;
	mon::TotalMemory totalMemoryResp;
	if (!stub)
		return totalMemoryCtrl;
	statusTotal = stub->GetTotalMemory(&context, empty, &totalMemoryResp);
	return totalMemoryResp;
}

MemoryMonitor::~MemoryMonitor()
{
	
}
