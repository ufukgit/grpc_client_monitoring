#include "diskchart.h"

DiskChart::DiskChart(QObject *parent, QWidget *chartBps, QWidget *chartIops, QWidget *chartSizeReq)
	: QObject(parent), uiChartBps(chartBps), uiChartIops(chartIops), uiChartSize(chartSizeReq)
{
	chart = new QChart();
	seriesRead = new QLineSeries();
	seriesRead->setName(" Read Speed (Mb/s) ");
	chart->addSeries(seriesRead);
	seriesWrite = new QLineSeries();
	seriesWrite->setName(" Write Speed (Mb/s) ");
	chart->addSeries(seriesWrite);
	seriesReadWrite = new QLineSeries();
	seriesReadWrite->setName(" Read Write (Mb) ");
	chart->addSeries(seriesReadWrite);
	chart->setTitle(" DISK MONITOR BPS ");
	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);
	chartView = new QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);
	axisX = new QDateTimeAxis;
	axisX->setTickCount(7);
	axisX->setFormat("hh:mm:ss");
	chart->addAxis(axisX, Qt::AlignBottom);
	seriesRead->attachAxis(axisX);
	seriesWrite->attachAxis(axisX);
	seriesReadWrite->attachAxis(axisX);
	axisY = new QValueAxis;
	chart->addAxis(axisY, Qt::AlignLeft);
	seriesRead->attachAxis(axisY);
	seriesReadWrite->setColor(Qt::red);
	seriesWrite->attachAxis(axisY);
	seriesReadWrite->attachAxis(axisY);
	axisY->setRange(0, 50000);
	chart->axisX()->setMin(QDateTime::currentDateTime().addSecs(-60*1));
	chart->axisX()->setMax(QDateTime::currentDateTime().addSecs(60*1));
	chartView->setRubberBand(QChartView::HorizontalRubberBand);
	chartView->setRubberBand(QChartView::VerticalRubberBand);
	QLinearGradient backgroundGradient;
	backgroundGradient.setStart(QPointF(0, 0));
	backgroundGradient.setFinalStop(QPointF(0, 1));
	backgroundGradient.setColorAt(0.0, QRgb(0xd2d0d1));
	backgroundGradient.setColorAt(1.0, QRgb(0x4c4547));
	backgroundGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
	chart->setBackgroundBrush(backgroundGradient);
	QLinearGradient plotAreaGradient;
	plotAreaGradient.setStart(QPointF(0, 1));
	plotAreaGradient.setFinalStop(QPointF(1, 0));
	plotAreaGradient.setColorAt(0.0, QRgb(0x555555));
	plotAreaGradient.setColorAt(1.0, QRgb(0x55aa55));
	plotAreaGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
	chart->setPlotAreaBackgroundBrush(plotAreaGradient);
	chart->setPlotAreaBackgroundVisible(true);

	
	chartIo = new QChart();
	seriesReadio = new QLineSeries();
	seriesReadio->setName(" Read Speed (Mb/s) ");
	chartIo->addSeries(seriesReadio);
	seriesWriteio = new QLineSeries();
	seriesWriteio->setName(" Write Speed (Mb/s) ");
	chartIo->addSeries(seriesWriteio);
	seriesReadWriteio = new QLineSeries();
	seriesReadWriteio->setName(" Read Write (Mb) ");
	chartIo->addSeries(seriesReadWriteio);
	chartIo->setTitle(" DISK MONITOR IOPS ");
	chartIo->legend()->setVisible(true);
	chartIo->legend()->setAlignment(Qt::AlignBottom);
	chartViewIo = new QChartView(chartIo);
	chartViewIo->setRenderHint(QPainter::Antialiasing);
	axisXIo = new QDateTimeAxis;
	axisXIo->setTickCount(7);
	axisXIo->setFormat("hh:mm:ss");
	chartIo->addAxis(axisXIo, Qt::AlignBottom);
	seriesReadio->attachAxis(axisXIo);
	seriesWriteio->attachAxis(axisXIo);
	seriesReadWriteio->attachAxis(axisXIo);
	axisYIo = new QValueAxis;
	chartIo->addAxis(axisYIo, Qt::AlignLeft);
	seriesReadio->attachAxis(axisYIo);
	seriesReadWriteio->setColor(Qt::red);
	seriesWriteio->attachAxis(axisYIo);
	seriesReadWriteio->attachAxis(axisYIo);
	axisYIo->setRange(0, 1024);
	axisXIo->setMin(QDateTime::currentDateTime().addSecs(-60*1));
	axisXIo->setMax(QDateTime::currentDateTime().addSecs(60*1));
	chartViewIo->setRubberBand(QChartView::HorizontalRubberBand);
	chartViewIo->setRubberBand(QChartView::VerticalRubberBand);
	chartIo->setBackgroundBrush(backgroundGradient);
	chartIo->setPlotAreaBackgroundBrush(plotAreaGradient);
	chartIo->setPlotAreaBackgroundVisible(true);
	
	chartSize = new QChart();
	seriesReadSize = new QLineSeries();
	seriesReadSize->setName(" Read Speed (Mb/s) ");
	chartSize->addSeries(seriesReadSize);
	seriesWriteSize = new QLineSeries();
	seriesWriteSize->setName(" Write Speed (Mb/s) ");
	chartSize->addSeries(seriesWriteSize);
	seriesReadWriteSize = new QLineSeries();
	seriesReadWriteSize->setName(" Read Write (Mb) ");
	chartSize->addSeries(seriesReadWriteSize);
	chartSize->setTitle(" DISK MONITOR SIZE ");
	chartSize->legend()->setVisible(true);
	chartSize->legend()->setAlignment(Qt::AlignBottom);
	chartViewSize = new QChartView(chartSize);
	chartViewSize->setRenderHint(QPainter::Antialiasing);
	axisXSize = new QDateTimeAxis;
	axisXSize->setTickCount(7);
	axisXSize->setFormat("hh:mm:ss");
	chartSize->addAxis(axisXSize, Qt::AlignBottom);
	seriesReadSize->attachAxis(axisXSize);
	seriesWriteSize->attachAxis(axisXSize);
	seriesReadWriteSize->attachAxis(axisXSize);
	axisYSize = new QValueAxis;
	chartSize->addAxis(axisYSize, Qt::AlignLeft);
	seriesReadSize->attachAxis(axisYSize);
	seriesReadWriteSize->setColor(Qt::red);
	seriesWriteSize->attachAxis(axisYSize);
	seriesReadWriteSize->attachAxis(axisYSize);
	axisYSize->setRange(0, 1024);
	axisXSize->setMin(QDateTime::currentDateTime().addSecs(-60*1));
	axisXSize->setMax(QDateTime::currentDateTime().addSecs(60*1));
	chartViewSize->setRubberBand(QChartView::HorizontalRubberBand);
	chartViewSize->setRubberBand(QChartView::VerticalRubberBand);
	chartSize->setBackgroundBrush(backgroundGradient);
	chartSize->setPlotAreaBackgroundBrush(plotAreaGradient);
	chartSize->setPlotAreaBackgroundVisible(true);
}

DiskChart::~DiskChart()
{
	delete chart;
	delete chartView;
	delete chartIo;
	delete chartViewIo;
	delete chartSize;
	delete chartViewSize;
	delete seriesRead;
	delete seriesWrite;
	delete seriesReadWrite;
	delete seriesReadio;
	delete seriesWriteio;
	delete seriesReadWriteio;
	delete seriesReadSize;
	delete seriesWriteSize;
	delete seriesReadWriteSize;
	delete uiChartBps;
	delete uiChartIops;
	delete uiChartSize;
	delete axisX;
	delete axisY;
	delete axisXIo;
	delete axisYIo;
	delete axisXSize;
	delete axisYSize;
}

void DiskChart::createChartDisk()
{
	chartView->setParent(uiChartBps);
	chartViewIo->setParent(uiChartIops);
	chartViewSize->setParent(uiChartSize);
	
}
