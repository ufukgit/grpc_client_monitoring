#include "monitorchart.h"
#include "ui_monitorchart.h"
#include <unistd.h>

MonitorChart::MonitorChart(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MonitorChart)
{
	ui->setupUi(this);
	ui->tabWidget->setTabText(0, "Disk Monitor");
	ui->tabWidget->setTabText(1, "Network Monitor");
	ui->tabWidget->setTabText(2, "Memory Monitor");
	ui->tabWidget->setTabText(3, "CPU Monitor");
	ui->diskList->setStyleSheet("""QListWidget{ background: grey; }""");
	ui->ifaceList->setStyleSheet("""QListWidget{ background: grey; }""");
	ui->connectButton->setStyleSheet("""QPushButton{ background: grey; }""");
	ui->tabWidget->setStyleSheet("""QTabWidget{ background: grey; }""");
	memoryChart = new MemoryChart(this, ui->freeFrame, ui->usedFrame, ui->cachedFrame, ui->swapFrame);
	memoryChart->createChartMem();
	memoryChart->chartViewMem->resize(ui->freeFrame->size());
	memoryChart->chartViewMemUsed->resize(ui->usedFrame->size());
	memoryChart->chartViewMemCached->resize(ui->cachedFrame->size());
	memoryChart->chartViewMemSwap->resize(ui->swapFrame->size());
	diskChart = new DiskChart(this, ui->diskBpsFrame, ui->diskIopsFrame, ui->diskSizeFrame);
	diskChart->createChartDisk();
	diskChart->chartView->resize(ui->diskBpsFrame->size());
	diskChart->chartViewIo->resize(ui->diskIopsFrame->size());
	diskChart->chartViewSize->resize(ui->diskIopsFrame->size());
	networkChart = new NetworkChart(this, ui->ifaceFrame, ui->ifacePacketFrame);
	networkChart->createChartNetwork();
	networkChart->chartViewIface->resize(ui->ifaceFrame->size());
	networkChart->chartViewIfacePacket->resize(ui->ifacePacketFrame->size());
	cpuChart = new CpuChart(this, ui->tab_4);
	cpuChart->createChartCpu();
}

void MonitorChart::on_connectButton_clicked()
{
	std::string address;
	address = ui->lineEdit->text().toStdString();
	
	diskMonitor = new DiskMonitor(address);
	if(!diskMonitor)
		return;
	
	if (diskMonitor->allDiskResp.resp_size() == 0) {
		QMessageBox::information(this, tr("ERROR"), tr("Server is not running"));
		return;
	}
	
	ui->ifaceList->clear();
	ui->diskList->clear();
	
	for(int k=0; k < diskMonitor->allDiskResp.resp_size(); k++) {
		QString diskname = QString::fromStdString(diskMonitor->allDiskResp.resp(k).name());
		ui->diskList->addItem(diskname);
	}
	
	networkMonitor = new NetworkMonitor(address);
	if(!networkMonitor)
		return;
	for(int i=0; i < networkMonitor->allIfaceResp.iface_size(); i++) {
		QString ifacename = QString::fromStdString(networkMonitor->allIfaceResp.iface(i).name());
		ui->ifaceList->addItem(ifacename);
	}
	
	memoryMonitor = new MemoryMonitor(address);
	if(!memoryMonitor)
		return;
	timerMemory = new QTimer(this);
	connect(timerMemory, SIGNAL(timeout()), this, SLOT(showMemory()));
	timerMemory->start(1000);
	
	cpuMonitor = new CpuMonitor(address);
	if(!cpuMonitor)
		return;
}

void MonitorChart::on_diskList_itemDoubleClicked(QListWidgetItem *item)
{
	networkChart->seriesReadIface->clear();
	networkChart->seriesWriteIface->clear();
	networkChart->seriesReadWriteIface->clear();
	networkChart->seriesReadIfacePacket->clear();
	networkChart->seriesWriteIfacePacket->clear();
	networkChart->seriesReadWriteIfacePacket->clear();
	diskChart->seriesRead->clear();
	diskChart->seriesWrite->clear();
	diskChart->seriesReadWrite->clear();
	diskChart->seriesReadio->clear();
	diskChart->seriesWriteio->clear();
	diskChart->seriesReadWriteio->clear();
	diskChart->seriesReadSize->clear();
	diskChart->seriesWriteSize->clear();
	diskChart->seriesReadWriteSize->clear();
	fileDisk = item->text().toStdString();
	timerBps = new QTimer(this);
	connect(timerBps, SIGNAL(timeout()), this, SLOT(showDiskMonitor()));
	timerBps->start(1000);
	countDiskClick++;
	if(countDiskClick >= 2) {
		if(timerBps->isActive())
			timerBps->stop();
	}
	file.clear();
	return;
}

void MonitorChart::showDiskMonitor()
{
	if(!file.empty()){
		fileDisk.clear();
		return;
	}
	qint64 bpsRead = diskMonitor->GetDiskBPS(fileDisk).value().r() / 1024;
	qint64 bpsWrite = diskMonitor->GetDiskBPS(fileDisk).value().w() / 1024;
	qint64 bpsRw = diskMonitor->GetDiskBPS(fileDisk).value().rw() / 1024;
	qint64 currentDate = QDateTime::currentMSecsSinceEpoch();
	diskChart->seriesRead->append(currentDate, bpsRead);
	diskChart->seriesWrite->append(currentDate, bpsWrite);
	diskChart->seriesReadWrite->append(currentDate, bpsRw);
	qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
	diskChart->chart->axisX()->setMin(QDateTime::currentDateTime().addSecs(-30));
	diskChart->chart->axisX()->setMax(QDateTime::currentDateTime().addSecs(30));
	diskChart->chart->update();
	diskChart->chartView->update();
	qint64 iopsRead = diskMonitor->GetDiskIOPS(fileDisk).value().r() / 1024;
	qint64 iopsWrite = diskMonitor->GetDiskIOPS(fileDisk).value().w() / 1024;
	qint64 iopsRw = diskMonitor->GetDiskIOPS(fileDisk).value().rw() / 1024;
	diskChart->seriesReadio->append(currentDate, iopsRead);
	diskChart->seriesWriteio->append(currentDate, iopsWrite);
	diskChart->seriesReadWriteio->append(currentDate, iopsRw);
	qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
	diskChart->chartIo->axisX()->setMin(QDateTime::currentDateTime().addSecs(-30));
	diskChart->chartIo->axisX()->setMax(QDateTime::currentDateTime().addSecs(30));
	diskChart->chartIo->update();
	diskChart->chartViewIo->update();
	qint64 ibprRead = diskMonitor->GetDiskRequestSizeInBytesPerRead(fileDisk).value().r() / 1024;
	qint64 ibprWrite = diskMonitor->GetDiskRequestSizeInBytesPerRead(fileDisk).value().w() / 1024;
	qint64 ibprRw = diskMonitor->GetDiskRequestSizeInBytesPerRead(fileDisk).value().rw() / 1024;
	diskChart->seriesReadSize->append(currentDate, ibprRead);
	diskChart->seriesWriteSize->append(currentDate, ibprWrite);
	diskChart->seriesReadWriteSize->append(currentDate, ibprRw);
	qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
	diskChart->chartSize->axisX()->setMin(QDateTime::currentDateTime().addSecs(-30));
	diskChart->chartSize->axisX()->setMax(QDateTime::currentDateTime().addSecs(30));
	diskChart->chartSize->update();
	diskChart->chartViewSize->update();
}

void MonitorChart::on_ifaceList_itemDoubleClicked(QListWidgetItem *item)
{
	diskChart->seriesRead->clear();
	diskChart->seriesWrite->clear();
	diskChart->seriesReadWrite->clear();
	diskChart->seriesReadio->clear();
	diskChart->seriesWriteio->clear();
	diskChart->seriesReadWriteio->clear();
	diskChart->seriesReadSize->clear();
	diskChart->seriesWriteSize->clear();
	diskChart->seriesReadWriteSize->clear();
	networkChart->seriesReadIface->clear();
	networkChart->seriesWriteIface->clear();
	networkChart->seriesReadWriteIface->clear();
	networkChart->seriesReadIfacePacket->clear();
	networkChart->seriesWriteIfacePacket->clear();
	networkChart->seriesReadWriteIfacePacket->clear();
	file = item->text().toStdString();
	timerFace = new QTimer(this);
	connect(timerFace, SIGNAL(timeout()), this, SLOT(showNetworkMonitor()));
	timerFace->start(1000);
	countIfaceClick++;
	if(countIfaceClick >= 2) {
		if(timerFace->isActive())
			timerFace->stop();
	}
	fileDisk.clear();
	return;
}

void MonitorChart::showNetworkMonitor()
{
	if(!fileDisk.empty()){
		file.clear();
		return;
	}
	qint64 ifaceRead = networkMonitor->GetIfaceBytesPerSecond(file).value().r() / 1024;
	qint64 ifaceWrite = networkMonitor->GetIfaceBytesPerSecond(file).value().w() / 1024;
	qint64 ifaceRw = networkMonitor->GetIfaceBytesPerSecond(file).value().rw() / 1024;
	qint64 currentDate = QDateTime::currentMSecsSinceEpoch();
	networkChart->seriesReadIface->append(currentDate, ifaceRead);
	networkChart->seriesWriteIface->append(currentDate, ifaceWrite);
	networkChart->seriesReadWriteIface->append(currentDate, ifaceRw);
	qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
	networkChart->chartIface->axisX()->setMin(QDateTime::currentDateTime().addSecs(-30));
	networkChart->chartIface->axisX()->setMax(QDateTime::currentDateTime().addSecs(30));
	networkChart->chartIface->update();
	networkChart->chartViewIface->update();
	qint64 ifacePacketRead = networkMonitor->GetIfacePacketsPerSecond(file).value().r();
	qint64 ifacePacketWrite = networkMonitor->GetIfacePacketsPerSecond(file).value().w();
	qint64 ifacePacketRw = networkMonitor->GetIfacePacketsPerSecond(file).value().rw();
	networkChart->seriesReadIfacePacket->append(currentDate, ifacePacketRead);
	networkChart->seriesWriteIfacePacket->append(currentDate, ifacePacketWrite);
	networkChart->seriesReadWriteIfacePacket->append(currentDate, ifacePacketRw);
	qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
	networkChart->chartIfacePacket->axisX()->setMin(QDateTime::currentDateTime().addSecs(-30));
	networkChart->chartIfacePacket->axisX()->setMax(QDateTime::currentDateTime().addSecs(30));
	networkChart->chartIfacePacket->update();
	networkChart->chartViewIfacePacket->update();
}

void MonitorChart::showMemory()
{
	memoryChart->seriesFree->clear();
	memoryChart->seriesUsed->clear();
	memoryChart->seriesCached->clear();
	memoryChart->seriesSwap->clear();
	qint64 free = memoryMonitor->GetFreeMemory().value();
	qint64 used = memoryMonitor->GetUsedMemory().value();
	qint64 cached = memoryMonitor->GetCachedMemory().value();
	qint64 swap = memoryMonitor->GetSwapMemory().value();
	qint64 total = memoryMonitor->GetTotalMemory().value();
	double percentFree = (100 * free )/ total;
	double percentUsed = (100 * used )/ total;
	double percentCached = (100 * cached )/ total;
	double percentSwap = (100 * swap )/ total;
	memoryChart->seriesFree->append("", 100 - percentFree);
	memoryChart->slice = memoryChart->seriesFree->append("Free Memory", percentFree);
	memoryChart->seriesUsed->append("", 100 - percentUsed);
	memoryChart->sliceUsed = memoryChart->seriesUsed->append("Used Memory", percentUsed);
	memoryChart->seriesCached->append("", 100 - percentCached);
	memoryChart->sliceCached = memoryChart->seriesCached->append("Cached Memory", percentCached);
	memoryChart->seriesSwap->append("", 100 - percentSwap);
	memoryChart->sliceSwap = memoryChart->seriesSwap->append("Swap Memory", percentSwap);
	memoryChart->chartViewMem->update();
	memoryChart->chartViewMem->chart()->update();
	memoryChart->chartViewMemUsed->update();
	memoryChart->chartViewMemUsed->chart()->update();
	memoryChart->chartViewMemCached->update();
	memoryChart->chartViewMemCached->chart()->update();
	memoryChart->chartViewMemSwap->update();
	memoryChart->chartViewMemSwap->chart()->update();
}

void MonitorChart::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::MiddleButton)
	{
		diskChart->chart->zoomReset();
		diskChart->chart->mapToValue(event->pos());
		diskChart->chartIo->zoomReset();
		diskChart->chartIo->mapToValue(event->pos());
		diskChart->chartSize->zoomReset();
		diskChart->chartSize->mapToValue(event->pos());
		networkChart->chartIface->zoomReset();
		networkChart->chartIface->mapToValue(event->pos());
		networkChart->chartIfacePacket->zoomReset();
		networkChart->chartIfacePacket->mapToValue(event->pos());
		event->accept();
	}
	QWidget::mousePressEvent(event);
}

MonitorChart::~MonitorChart()
{
	delete ui;
	delete networkMonitor;
	delete diskMonitor;
	delete cpuMonitor;
	delete memoryMonitor;
	delete memoryChart;
	delete diskChart;
	delete networkChart;
	delete cpuChart;
	delete timerBps;
	delete timerFace;
	delete timerMemory;
	delete timerBps;
	delete timerFace;
	delete timerMemory;
	delete cpuMonitor;
	delete diskMonitor;
	delete memoryMonitor;
	delete networkMonitor;
}

