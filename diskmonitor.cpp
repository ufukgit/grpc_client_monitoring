#include "diskmonitor.h"

DiskMonitor::DiskMonitor(const std::string &ep)
{
	std::string endpoint = ep;
	CreateChannel(endpoint);
	GetBPS();
	GetIOPS();
	GetRequestSizeInBytesPerRead();
}

void DiskMonitor::CreateChannel(const std::string &ep)
{
	using grpc::Channel;
	std::shared_ptr<Channel> channel;
	channel = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	stub = DiskMonitoring::NewStub(channel);
}

mon::AllDiskResponse DiskMonitor::GetBPS()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusDisk;
	mon::AllDiskResponse allDiskRespCtrl;
	if (!stub) 
		return allDiskRespCtrl;
	statusDisk = stub->GetBPS(&context, empty, &allDiskResp); 
	return allDiskResp;
}

mon::DiskResponse DiskMonitor::GetDiskBPS(std::string diskname)
{
	grpc::ClientContext context;
	grpc::Status statusDiskBytes;
	mon::DiskResponse diskResponseCtrl;
	mon::DiskResponse diskResponse;
	mon::DiskQ diskQ;
	if (!stub)
		return diskResponseCtrl;
	*diskQ.mutable_name() = diskname;
	statusDiskBytes = stub->GetDiskBPS(&context, diskQ, &diskResponse);
	return diskResponse;
}

mon::AllDiskResponse DiskMonitor::GetIOPS()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusDisk;
	mon::AllDiskResponse allDiskRespCtrl;
	mon::AllDiskResponse allDiskIoResp;
	if (!stub)
		return allDiskRespCtrl;
	statusDisk = stub->GetIOPS(&context, empty, &allDiskIoResp);
	return allDiskIoResp;
}

mon::DiskResponse DiskMonitor::GetDiskIOPS(std::string diskname)
{
	grpc::ClientContext context;
	grpc::Status statusDiskBytes;
	mon::DiskResponse diskResponseCtrl;
	mon::DiskResponse diskResponse;
	mon::DiskQ diskQ;
	if (!stub)
		return diskResponseCtrl;
	*diskQ.mutable_name() = diskname;
	statusDiskBytes = stub->GetDiskIOPS(&context, diskQ, &diskResponse);
	return diskResponse;
}

mon::AllDiskResponse DiskMonitor::GetRequestSizeInBytesPerRead()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusDisk;
	mon::AllDiskResponse allDiskRespCtrl;
	mon::AllDiskResponse allDiskRequestResp;
	if (!stub)
		return allDiskRespCtrl;
	statusDisk = stub->GetRequestSizeInBytesPerRead(&context, empty, &allDiskRequestResp); 
	return allDiskRequestResp;
}

mon::DiskResponse DiskMonitor::GetDiskRequestSizeInBytesPerRead(std::string diskname)
{
	grpc::ClientContext context;
	grpc::Status statusDiskBytes;
	mon::DiskResponse diskResponseCtrl;
	mon::DiskResponse diskResponse;
	mon::DiskQ diskQ;
	if (!stub)
		return diskResponseCtrl;
	*diskQ.mutable_name() = diskname;
	statusDiskBytes = stub->GetDiskRequestSizeInBytesPerRead(&context, diskQ, &diskResponse);
	return diskResponse;
}

DiskMonitor::~DiskMonitor()
{
	
}
