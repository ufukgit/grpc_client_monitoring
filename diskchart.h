#ifndef DISKCHART_H
#define DISKCHART_H

#include <QObject>
#include <QPieSeries>
#include <QtCharts>
#include <QPieSlice>

class DiskChart : public QObject
{
	Q_OBJECT
public:
	DiskChart(QObject *parent, QWidget *chartBps, QWidget *chartIops, QWidget *chartSizeReq);
	~DiskChart();
	QChart *chart;
	QChartView *chartView;
	QChart *chartIo;
	QChartView *chartViewIo;
	QChart *chartSize;
	QChartView *chartViewSize;
	QLineSeries *seriesRead;
	QLineSeries *seriesWrite;
	QLineSeries *seriesReadWrite;
	QLineSeries *seriesReadio;
	QLineSeries *seriesWriteio;
	QLineSeries *seriesReadWriteio;
	QLineSeries *seriesReadSize;
	QLineSeries *seriesWriteSize;
	QLineSeries *seriesReadWriteSize;
	void createChartDisk();
	
private:
	QWidget *uiChartBps;
	QWidget *uiChartIops;
	QWidget *uiChartSize;
	QDateTimeAxis *axisX;
	QValueAxis *axisY;
	QDateTimeAxis *axisXIo;
	QValueAxis *axisYIo;
	QDateTimeAxis *axisXSize;
	QValueAxis *axisYSize;
signals:
	
};

#endif // DISKCHART_H
