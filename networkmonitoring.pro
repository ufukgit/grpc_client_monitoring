QT       += core gui
QT       += core gui charts


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

INCLUDEPATH += /usr/local/
LIBS += -lgrpc++
LIBS += /usr/local/lib/libprotobuf.a

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    cpuchart.cpp \
    cpumonitor.cpp \
    diskchart.cpp \
    diskmonitor.cpp \
    main.cpp \
    memorychart.cpp \
    memorymonitor.cpp \
    monitorchart.cpp \
    monitoring.grpc.pb.cc \
    monitoring.pb.cc \
    networkchart.cpp \
    networkmonitor.cpp

HEADERS += \
    cpuchart.h \
    cpumonitor.h \
    diskchart.h \
    diskmonitor.h \
    memorychart.h \
    memorymonitor.h \
    monitorchart.h \
    monitoring.grpc.pb.h \
    monitoring.pb.h \
    networkchart.h \
    networkmonitor.h

FORMS += \
    monitorchart.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    monitoring.proto

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/release/ -labsl_synchronization
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/debug/ -labsl_synchronization
else:unix: LIBS += -L$$PWD/../../../../../../usr/local/lib/ -labsl_synchronization

INCLUDEPATH += $$PWD/../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../usr/local/include
