#ifndef NETWORKCHART_H
#define NETWORKCHART_H

#include <QObject>
#include <QPieSeries>
#include <QtCharts>
#include <QPieSlice>

class NetworkChart : public QObject
{
	Q_OBJECT
public:
	NetworkChart(QObject *parent, QWidget *chartInterface, QWidget *chartInterfacePacket);
	~NetworkChart();
	QLineSeries *seriesReadIface;
	QLineSeries *seriesWriteIface;
	QLineSeries *seriesReadWriteIface;
	QLineSeries *seriesReadIfacePacket;
	QLineSeries *seriesWriteIfacePacket;
	QLineSeries *seriesReadWriteIfacePacket;
	QChart *chartIface;
	QChartView *chartViewIface;
	QChart *chartIfacePacket;
	QChartView *chartViewIfacePacket;
	void createChartNetwork();
	
private:
	QWidget *uiChartInterface;
	QWidget *uiChartInterfacePacket;
	QDateTimeAxis *axisXIface;
	QValueAxis *axisYIface;
	QDateTimeAxis *axisXIfacePacket;
	QValueAxis *axisYIfacePacket;
	
signals:
	
};

#endif // NETWORKCHART_H
