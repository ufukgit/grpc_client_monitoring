#include "cpumonitor.h"

CpuMonitor::CpuMonitor(const std::string &ep)
{
	std::string endpoint = ep;
	CreateChannel(endpoint);
	GetUsagePercentage();
}

void CpuMonitor::CreateChannel(const std::string &ep)
{
	using grpc::Channel;
	std::shared_ptr<Channel> channel;
	channel = grpc::CreateChannel(ep, grpc::InsecureChannelCredentials());
	stub = CpuMonitoring::NewStub(channel);
}

mon::CpuResponse CpuMonitor::GetUsagePercentage()
{
	google::protobuf::Empty empty;
	grpc::ClientContext context;
	grpc::Status statusCpu;
	mon::CpuResponse cpuRespCtrl;
	mon::CpuResponse cpuResponse;
	if (!stub)
		return cpuRespCtrl;
	statusCpu = stub->GetUsagePercentage(&context, empty, &cpuResponse);
	return cpuResponse;
}

CpuMonitor::~CpuMonitor()
{
	
}
