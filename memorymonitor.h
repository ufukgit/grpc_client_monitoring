#ifndef MEMORYMONITOR_H
#define MEMORYMONITOR_H

#include <grpc++/grpc++.h>
#include "monitoring.grpc.pb.h"
#include <google/protobuf/empty.pb.h>
#include <QObject>
#include <QDebug>
#include <QTimer>

using mon::MemoryMonitoring;

class MemoryMonitor : public QObject
{
	Q_OBJECT
public:
	MemoryMonitor(const std::string &ep);
	~MemoryMonitor();
	mon::FreeMemory GetFreeMemory();
	mon::UsedMemory GetUsedMemory();
	mon::CachedMemory GetCachedMemory();
	mon::SwapMemory GetSwapMemory();
	mon::TotalMemory GetTotalMemory();
	
private:
	std::unique_ptr<MemoryMonitoring::Stub> stub = nullptr;
	void CreateChannel(const std::string &ep);
};

#endif // MEMORYMONITOR_H
