#include "networkchart.h"

NetworkChart::NetworkChart(QObject *parent, QWidget *chartInterface, QWidget *chartInterfacePacket)
	:  QObject(parent), uiChartInterface(chartInterface), uiChartInterfacePacket(chartInterfacePacket)
{
	chartIface = new QChart();
	seriesReadIface = new QLineSeries();
	seriesReadIface->setName(" Read Speed (Mb/s) ");
	chartIface->addSeries(seriesReadIface);
	seriesWriteIface = new QLineSeries();
	seriesWriteIface->setName(" Write Speed (Mb/s) ");
	chartIface->addSeries(seriesWriteIface);
	seriesReadWriteIface = new QLineSeries();
	seriesReadWriteIface->setName(" Read Write (Mb) ");
	chartIface->addSeries(seriesReadWriteIface);
	chartIface->setTitle(" NETWORK MONITOR IFACE ");
	chartIface->legend()->setVisible(true);
	chartIface->legend()->setAlignment(Qt::AlignBottom);
	chartViewIface = new QChartView(chartIface);
	chartViewIface->setRenderHint(QPainter::Antialiasing);
	axisXIface = new QDateTimeAxis;
	axisXIface->setTickCount(7);
	axisXIface->setFormat("hh:mm:ss");
	chartIface->addAxis(axisXIface, Qt::AlignBottom);
	seriesReadIface->attachAxis(axisXIface);
	seriesWriteIface->attachAxis(axisXIface);
	seriesReadWriteIface->attachAxis(axisXIface);
	axisYIface = new QValueAxis;
	chartIface->addAxis(axisYIface, Qt::AlignLeft);
	seriesReadIface->attachAxis(axisYIface);
	seriesReadWriteIface->setColor(Qt::red);
	seriesWriteIface->attachAxis(axisYIface);
	seriesReadWriteIface->attachAxis(axisYIface);
	axisYIface->setRange(0, 8192);
	chartIface->axisX()->setMin(QDateTime::currentDateTime().addSecs(-60*1));
	chartIface->axisX()->setMax(QDateTime::currentDateTime().addSecs(60*1));
	chartViewIface->setRubberBand(QChartView::HorizontalRubberBand);
	chartViewIface->setRubberBand(QChartView::VerticalRubberBand);
	chartIface->setPlotAreaBackgroundVisible(true);
	QLinearGradient backgroundGradient;
	backgroundGradient.setStart(QPointF(0, 0));
	backgroundGradient.setFinalStop(QPointF(0, 1));
	backgroundGradient.setColorAt(0.0, QRgb(0xd2d0d1));
	backgroundGradient.setColorAt(1.0, QRgb(0x4c4547));
	backgroundGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
	chartIface->setBackgroundBrush(backgroundGradient);
	QLinearGradient plotAreaGradient;
	plotAreaGradient.setStart(QPointF(0, 1));
	plotAreaGradient.setFinalStop(QPointF(1, 0));
	plotAreaGradient.setColorAt(0.0, QRgb(0x555555));
	plotAreaGradient.setColorAt(1.0, QRgb(0x55aa55));
	plotAreaGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
	chartIface->setPlotAreaBackgroundBrush(plotAreaGradient);
	chartIface->setPlotAreaBackgroundVisible(true);
	
	chartIfacePacket = new QChart();
	seriesReadIfacePacket = new QLineSeries();
	seriesReadIfacePacket->setName(" Read Speed (Mb/s) ");
	chartIfacePacket->addSeries(seriesReadIfacePacket);
	seriesWriteIfacePacket = new QLineSeries();
	seriesWriteIfacePacket->setName(" Write Speed (Mb/s) ");
	chartIfacePacket->addSeries(seriesWriteIfacePacket);
	seriesReadWriteIfacePacket = new QLineSeries();
	seriesReadWriteIfacePacket->setName(" Read Write (Mb) ");
	chartIfacePacket->addSeries(seriesReadWriteIfacePacket);
	chartIfacePacket->setTitle(" NETWORK MONITOR IFACE PACKET ");
	chartIfacePacket->legend()->setVisible(true);
	chartIfacePacket->legend()->setAlignment(Qt::AlignBottom);
	chartViewIfacePacket = new QChartView(chartIfacePacket);
	chartViewIfacePacket->setRenderHint(QPainter::Antialiasing);
	axisXIfacePacket = new QDateTimeAxis;
	axisXIfacePacket->setTickCount(7);
	axisXIfacePacket->setFormat("hh:mm:ss");
	chartIfacePacket->addAxis(axisXIfacePacket, Qt::AlignBottom);
	seriesReadIfacePacket->attachAxis(axisXIfacePacket);
	seriesWriteIfacePacket->attachAxis(axisXIfacePacket);
	seriesReadWriteIfacePacket->attachAxis(axisXIfacePacket);
	axisYIfacePacket = new QValueAxis;
	chartIfacePacket->addAxis(axisYIfacePacket, Qt::AlignLeft);
	seriesReadIfacePacket->attachAxis(axisYIfacePacket);
	seriesReadWriteIfacePacket->setColor(Qt::red);
	seriesWriteIfacePacket->attachAxis(axisYIfacePacket);
	seriesReadWriteIfacePacket->attachAxis(axisYIfacePacket);
	axisYIfacePacket->setRange(0, 4096);
	chartIfacePacket->axisX()->setMin(QDateTime::currentDateTime().addSecs(-60*1));
	chartIfacePacket->axisX()->setMax(QDateTime::currentDateTime().addSecs(60*1));
	chartViewIfacePacket->setRubberBand(QChartView::HorizontalRubberBand);
	chartViewIfacePacket->setRubberBand(QChartView::VerticalRubberBand);
	chartIfacePacket->setPlotAreaBackgroundVisible(true);
	chartIfacePacket->setBackgroundBrush(backgroundGradient);
	chartIfacePacket->setPlotAreaBackgroundBrush(plotAreaGradient);
	chartIfacePacket->setPlotAreaBackgroundVisible(true);
}

NetworkChart::~NetworkChart()
{
	delete seriesReadIface;
	delete seriesWriteIface;
	delete seriesReadWriteIface;
	delete seriesReadIfacePacket;
	delete seriesWriteIfacePacket;
	delete seriesReadWriteIfacePacket;
	delete chartIface;
	delete chartViewIface;
	delete chartIfacePacket;
	delete chartViewIfacePacket;
	delete uiChartInterface;
	delete uiChartInterfacePacket;
	delete axisXIface;
	delete axisYIface;
	delete axisXIfacePacket;
	delete axisYIfacePacket;
}

void NetworkChart::createChartNetwork()
{
	chartViewIface->setParent(uiChartInterface);
	chartViewIfacePacket->setParent(uiChartInterfacePacket);
}
