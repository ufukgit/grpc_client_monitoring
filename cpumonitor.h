#ifndef CPUMONITOR_H
#define CPUMONITOR_H

#include <grpc++/grpc++.h>
#include "monitoring.grpc.pb.h"
#include <google/protobuf/empty.pb.h>
#include <QObject>
#include <QDebug>
#include <QTimer>

using mon::CpuMonitoring;

class CpuMonitor : public QObject
{
	Q_OBJECT
public:
	CpuMonitor(const std::string &ep);
	~CpuMonitor();
	mon::CpuResponse GetUsagePercentage();
	
private:
	std::unique_ptr<CpuMonitoring::Stub> stub = nullptr;
	void CreateChannel(const std::string &ep);
	
};

#endif // CPUMONITOR_H
