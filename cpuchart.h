#ifndef CPUCHART_H
#define CPUCHART_H

#include <QObject>
#include <QPieSeries>
#include <QtCharts>
#include <QPieSlice>
#include <cpumonitor.h>

class CpuChart : public QObject
{
	Q_OBJECT
public:
	CpuChart(QObject *parent, QWidget *chartCpuUsage);
	~CpuChart();
	QPieSeries *seriesCpu;
	QChartView *chartViewCpu;
	QPieSlice *sliceCpu;
	void createChartCpu();
	QMap<int, QChart*> chartMap;
	
private:
	QWidget *uiChartCpuUsage;
	int x=0;
	int y=0;
	CpuMonitor *cpuMonitor = nullptr;
	
};

#endif // CPUCHART_H
