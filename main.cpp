#include "cpumonitor.h"
#include "memorymonitor.h"
#include "networkmonitor.h"
#include "monitorchart.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MonitorChart mc;
	mc.show();
	return a.exec();
}
