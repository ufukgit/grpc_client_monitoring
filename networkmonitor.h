#ifndef NETWORKMONITOR_H
#define NETWORKMONITOR_H

#include <grpc++/grpc++.h>
#include "monitoring.grpc.pb.h"
#include <google/protobuf/empty.pb.h>
#include <QObject>
#include <QDebug>

using mon::NetworkMonitoring;

class NetworkMonitor : public QObject
{
	Q_OBJECT
public:
	NetworkMonitor(const std::string &ep);
	~NetworkMonitor();
	mon::IfaceResponse GetIfaceBytesPerSecond(std::string ifacename);
	mon::IfaceResponse GetIfacePacketsPerSecond(std::string ifacename);
	mon::AllIfaceResponses allIfaceResp;
private slots:
private:
	std::unique_ptr<NetworkMonitoring::Stub> stub = nullptr;
	void CreateChannel(const std::string &ep);
	mon::AllIfaceResponses GetBytesPerSecond();
	mon::AllIfaceResponses GetPacketsPerSecond();
};

#endif // NETWORKMONITOR_H
