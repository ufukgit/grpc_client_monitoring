#include "memorychart.h"

MemoryChart::MemoryChart(QObject *parent, QWidget *chartFree, QWidget *chartUsed, QWidget *chartCached, QWidget *chartSwap) 
	: QObject(parent), uiChartFree(chartFree), uiChartUsed(chartUsed), uiChartCached(chartCached), uiChartSwap(chartSwap)
{
	seriesFree = new QPieSeries();
	seriesFree->setHoleSize(0.40);
	slice = new QPieSlice();
	chartViewMem = new QChartView();
	chartViewMem->setRenderHint(QPainter::Antialiasing);
	chartViewMem->chart()->setTitle("Free Memory");
	chartViewMem->chart()->addSeries(seriesFree);
	chartViewMem->chart()->setTheme(QChart::ChartThemeBlueIcy);
	chartViewMem->chart()->legend()->hide();
	seriesUsed = new QPieSeries();
	seriesUsed->setHoleSize(0.40);
	sliceUsed = new QPieSlice();
	chartViewMemUsed = new QChartView();
	chartViewMemUsed->setRenderHint(QPainter::Antialiasing);
	chartViewMemUsed->chart()->setTitle("Used Memory");
	chartViewMemUsed->chart()->addSeries(seriesUsed);
	chartViewMemUsed->chart()->setTheme(QChart::ChartThemeBlueIcy);
	chartViewMemUsed->chart()->legend()->hide();
	seriesCached = new QPieSeries();
	seriesCached->setHoleSize(0.40);
	sliceCached = new QPieSlice();
	chartViewMemCached = new QChartView();
	chartViewMemCached->setRenderHint(QPainter::Antialiasing);
	chartViewMemCached->chart()->setTitle("Cached Memory");
	chartViewMemCached->chart()->addSeries(seriesCached);
	chartViewMemCached->chart()->setTheme(QChart::ChartThemeBlueIcy);
	chartViewMemCached->chart()->legend()->hide();
	seriesSwap = new QPieSeries();
	seriesSwap->setHoleSize(0.40);
	sliceSwap = new QPieSlice();
	chartViewMemSwap = new QChartView();
	chartViewMemSwap->setRenderHint(QPainter::Antialiasing);
	chartViewMemSwap->chart()->setTitle("Swap Memory");
	chartViewMemSwap->chart()->addSeries(seriesSwap);
	chartViewMemSwap->chart()->setTheme(QChart::ChartThemeBlueIcy);
	chartViewMemSwap->chart()->legend()->hide();
}

MemoryChart::~MemoryChart()
{
	delete seriesFree;
	delete chartViewMem;
	delete slice;
	delete seriesUsed;
	delete chartViewMemUsed;
	delete sliceUsed;
	delete seriesCached;
	delete chartViewMemCached;
	delete sliceCached;
	delete seriesSwap;
	delete chartViewMemSwap;
	delete sliceSwap;
	delete uiChartFree;
	delete uiChartUsed;
	delete uiChartCached;
	delete uiChartSwap;
}

void MemoryChart::createChartMem()
{
	chartViewMem->setParent(uiChartFree);
	chartViewMemUsed->setParent(uiChartUsed);
	chartViewMemCached->setParent(uiChartCached);
	chartViewMemSwap->setParent(uiChartSwap);
}
