#include "cpuchart.h"
#include "cpumonitor.h"
#include "ui_monitorchart.h"
#include "monitorchart.h"

CpuChart::CpuChart(QObject *parent, QWidget *chartCpuUsage)
	: QObject(parent), uiChartCpuUsage(chartCpuUsage)
{
}

CpuChart::~CpuChart()
{
	delete seriesCpu;
	delete chartViewCpu;
	delete sliceCpu;
}

void CpuChart::createChartCpu()
{
	std::string address="10.5.5.0:30561";
	cpuMonitor = new CpuMonitor(address);
	if(!cpuMonitor)
		return;
	Ui_MonitorChart *uiMon = new Ui_MonitorChart();
	if(!uiMon)
		return;
	for (int i = 1; i < cpuMonitor->GetUsagePercentage().value_size(); i++) {
		qDebug() << "i:" << i;
		QPieSeries *seriesCore = new QPieSeries();
		QChart *chartCore = new QChart();
		QChartView *chartViewCore =new QChartView(chartCore);
		QFrame *horizontalFrame =  new QFrame(uiMon->tab_4);
		QHBoxLayout *horizontalLayout = new QHBoxLayout(horizontalFrame);
		horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
		chartCore->addSeries(seriesCore);
		QTimer *timerCore2 = new QTimer(this);
		connect(timerCore2, &QTimer::timeout, [=]()
		{
			seriesCore->append("Core Usage", cpuMonitor->GetUsagePercentage().value(i));
		});
		chartViewCore->setRenderHint(QPainter::Antialiasing);
		chartViewCore->setParent(horizontalFrame);
		QLinearGradient backgroundGradient;
		backgroundGradient.setStart(QPointF(0, 0));
		backgroundGradient.setFinalStop(QPointF(0, 1));
		backgroundGradient.setColorAt(0.0, QRgb(0xd2d0d1));
		backgroundGradient.setColorAt(1.0, QRgb(0x4c4547));
		backgroundGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
		chartCore->setBackgroundBrush(backgroundGradient);
		QLinearGradient plotAreaGradient;
		plotAreaGradient.setStart(QPointF(0, 1));
		plotAreaGradient.setFinalStop(QPointF(1, 0));
		plotAreaGradient.setColorAt(0.0, QRgb(0x555555));
		plotAreaGradient.setColorAt(1.0, QRgb(0x55aa55));
		plotAreaGradient.setCoordinateMode(QGradient::ObjectBoundingMode);
		chartCore->setPlotAreaBackgroundBrush(plotAreaGradient);
		chartCore->setPlotAreaBackgroundVisible(true);
		horizontalFrame->setGeometry(QRect(x, y, 270, 375));
		chartViewCore->resize(horizontalFrame->size());
		if(i < 5) {
			x = x + 270;
		}
		if(i >= 4) {
			y = 375;
			x = x - 270;
		}
		timerCore2->start(5000);
	}
	return;
}
