#ifndef MONITORCHART_H
#define MONITORCHART_H

#include <QWidget>
#include "networkmonitor.h"
#include <QListWidgetItem>
#include "diskmonitor.h"
#include <QPieSeries>
#include <QtCharts>
#include <QPieSlice>
#include <QMap>
#include "cpumonitor.h"
#include "memorymonitor.h"
#include "memorychart.h"
#include "diskchart.h"
#include "networkchart.h"
#include "cpuchart.h"

namespace Ui {
class MonitorChart;
}

class MonitorChart : public QWidget
{
	Q_OBJECT
	
public:
	explicit MonitorChart(QWidget *parent = nullptr);
	~MonitorChart();
	
private slots:
	void on_connectButton_clicked();
	void on_diskList_itemDoubleClicked(QListWidgetItem *item);
	void on_ifaceList_itemDoubleClicked(QListWidgetItem *item);
	void showDiskMonitor();
	void showNetworkMonitor();
	void showMemory();
	
private:
	Ui::MonitorChart *ui;
	NetworkMonitor *networkMonitor = nullptr;
	DiskMonitor *diskMonitor = nullptr;
	CpuMonitor *cpuMonitor = nullptr;
	MemoryMonitor *memoryMonitor = nullptr;
	MemoryChart *memoryChart = nullptr;
	DiskChart *diskChart = nullptr;
	NetworkChart *networkChart = nullptr;
	CpuChart *cpuChart = nullptr;
	std::string fileDisk;
	std::string file;
	int countDiskClick=0;
	int countIfaceClick=0;
	QTimer *timerBps;
	QTimer *timerFace;
	QTimer *timerMemory;
	void mousePressEvent(QMouseEvent *event);
	int count=0;
	int x = 0;
	int y = 0;
};

#endif // MONITORCHART_H
